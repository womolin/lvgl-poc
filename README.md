# lvgl-poc

This project is based on the [LVGL simulator project](https://github.com/lvgl/lv_sim_eclipse_sdl).


## Setup

Clone the project and the related sub modules:

```
git clone --recursive git@gitlab.com:womolin/lvgl-poc.git
```

The [LVGL simulator project README](https://github.com/lvgl/lv_sim_eclipse_sdl/blob/master/README.md) describes required dependencies, build steps, and IDE setup.
